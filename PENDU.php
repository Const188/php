<?php

# **************************************************************************** #
#                        Variables Globales                                    #
#                                                                              #
#                                                                              #
#                                                                              #
# **************************************************************************** #

#$nbTours = 12;
#$lettrejoue = array();
#$taille = strlen($mot);


/*
 *  Demander le motCache
 * Afficher le menu demandeLettre
 * Utiliser la fonction estDans
 */

# **************************************************************************** #
#                                                                              #
#                    Fonctions                                                 #
#                                                                              #
#                                                                              #
# **************************************************************************** #
function estDans (string $mot , string $lettre) : int {
    $i = 0;
    while ($i < strlen($mot) ) {
        if($mot[$i] == $lettre) {
            return $i;
        }  
        $i = $i + 1;            
    }
    return -1;
}

function menuQuestionUneFois(array $tab) : int {
    
    $i = 0;
 $n = count($tab);
    
    while ($i < $n) {
        echo $i . " " .  $tab[$i] . "\n";
        $i++;
    }
    $indiceMax = $n - 1;
    
    echo "Choisir un indice compris entre 0 et $indiceMax \n";
    $ret = trim(fgets(STDIN));
    return $ret ;
    
    
}

/*
 * Fonction menu : 
 *  Demande à l'utilisateur de saisir un indice compris dans la plage d'indice du tableau tant
 *  que la saisie n'est pas bonne.
 * Paramètre : Tableau de chaine de caractères
 * Retour : 
 *  - Si l'utilisateur saisi un indice compris entre 0 et l'indice maximum du tableau
 *    retourne cet indice.
 * 
 */
 function menu (array $tab) :  int {
     
     while(true){
         
         $saisie = menuQuestionUneFois($tab);
        
         return  $ret;
         
     }
 }
 
 

 
/*La fonction affichage prend en paramètre un mot caché et 
 * un tableau des lettres jouées.
 * Elle retourne une chaîne de caractères à afficher
 * 
 * Exemple pour aucune lettre trouvée:
 * poupou
 * _ _ _ _ _ _
 * 
 * Exemple pour le p trouvé :
 * P _ _ P _ _
 * 
 * 
 * parcours le mot caractères par caractères
 * pour chaques caractères du mot on vérifie si il est présent dans les lettres
 * jouées :
 * - Si oui ajouter la lettre a ret 
 * - Si non remplacer la lettre par un _
 * 
 */
 
 
   function affichage ($taille, $i) {
 /*
       $i =  0;
   $taille = strlen$mot;
 while ( $i < $taille ){
     echo"_";
     $i=$i+1;
     
 }    
       
    */
    
  
}

function programmePrincipal() {
    
    
    $menuJouerOuPas = ["Jouer?", "Arreter?"];
    menuQuestionUneFois($menuJouerOuPas);
    
    
    
    echo "Saisir le mot caché :";
    $mot= trim(fgets(STDIN));
    
    echo "Saisir une lettre :";
    $lettre = trim(fgets(STDIN));
    
    echo strval(estDans ($mot , $lettre ));
    
    
    
}

# **************************************************************************** #
#                                                                              #
#                 Programme Principale                                         #
#                                                                              #
#                                                                              #
# **************************************************************************** #        

programmePrincipal();
?>